docker build -t androidprogrammer/demo:latest -t androidprogrammer/demo:$SHA -f ./client/Dockerfile ./client
docker build -t androidprogrammer/demo-servers:latest -t androidprogrammer/demo-servers:$SHA -f ./server/Dockerfile ./server
docker build -t androidprogrammer/demo-workers:latest -t androidprogrammer/demo-workers:$SHA -f ./worker/Dockerfile ./worker
docker push androidprogrammer/demo:latest
docker push androidprogrammer/demo-servers:latest
docker push androidprogrammer/demo-workers:latest

docker push androidprogrammer/demo:$SHA
docker push androidprogrammer/demo-servers:$SHA
docker push androidprogrammer/demo-workers:$SHA

kubectl apply -f k8s/
kubectl set image deployments/client-deployment cllient=androidprogrammer/demo:$SHA
kubectl set image deployments/server-deployment server=androidprogrammer/demo-servers:$SHA
kubectl set image deployments/worker-deployment wirjer=androidprogrammer/demo-workers:$SHA