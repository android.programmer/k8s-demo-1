# k8s-demo-1

## Introduction
How should you run kubernetes ?
Should you host it yourself? On cloud instances? On bare-metal server?
Should you use a managed Kubernetes service
### The 3 revolutions
1. Cloud infrastructure
2. Devops
3. Containers

These 3 combines together create cloud native world, Kubernetes is the implementation of the cloud native operating system. 

>Kubernetes does the things that the very best system administrator would do:
automation, failover, centralized logging, monitoring. It takes what we’ve learned
in the DevOps community and makes it the default, out of the box.
—Kelsey Hightower

### Some considerations before deciding self-hosted K8s

- Is the control plane highly available? That is, if a master node goes down or
becomes unresponsive, does your cluster still work? Can you still deploy or
update apps? Will your running applications still be fault-tolerant without the
control plane?

- Is your pool of wokrer nodes highly available ?

- Is your cluster set up securely ?

- Are all services in your cluster secure ?

- Is your cluster conformant ?

- Are your cluster nodes fully config-managed ?

- Is the data in your cluster properly backed up, including any persistent storage ? what is your restore process ? how ofthen do you test restores ?

- Once you have a working cluster, how do you maintain it over time ? How do you provision new nodes? Roll out config changes to existing nodes? Roll out K8s updates? scale in response to demand? enforce policies? 


https://twitter.com/copyconstruct/status/1020880388464377856

#### Resiliency testing tools

https://github.com/netflix/chaosmonkey

### Designing Infrastructure Applications
#### reconciler pattern
```mermaid
graph LR
A[actual state]-->B[expected state]
```

- API - Desired state changes to reconcile are sent to the software via the API instead of static code repo.
- Good infrasture operator = good software enigneer
- A more elegant and cloud native approach to solving this problem is to make the
(usually correct) assumption that whoever is attempting to bootstrap infrastructure
software has a local machine that we can use to our advantage. The existing machine
(your computer) serves as the first deployment tool, to create infrastructure in a
cloud automatically. After the infrastructure is in place, your local deployment tool
can then deploy itself to the newly created infrastructure and continually run. Good
deployment tools will allow you to easily clean this up when you are done.
- Storage meidum (state store)
- It is also important to remember
that making changes to a system does not happen instantly, but rather over time

### Deployment strategy
###

### Best practise for operating containers

- Use the native logging mechanisms of containers
- Ensure that your containers are stateless and immutable
- Avoid privileged containers
- Make your application easy to monitor
- Expose the health of your application
- Avoid running as root
- Carefully choose the image version

https://cloud.google.com/solutions/best-practices-for-operating-containers#log_aggregator_sidecar_pattern

### Kubeadm Quick start 
https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/install-kubeadm/


```
## turn of swap and set bridge-nf-call-iptables 
swapoff -a
sysctl net.bridge.bridge-nf-call-iptables =1 
```